
import flask
import csv
import flask_restful
from requests import get
from io import StringIO
from flask import Flask
from flask_restful import Resource, Api
from pymongo import MongoClient
import os
import json



# Instantiate the app
app = Flask(__name__)
api = Api(app)

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.brev

def welcome():
    flask.render_template(hello)
@api.representation('text/csv')
def output_csv(data, code, headers=None):
    ret=""
    with StringIO("") as csvfile:
        fieldnames = ['km', 'Close', "Open"]
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        for row in data:
            writer.writerow(row)
        csvfile.seek(0)
        line = csvfile.readline()
        while line:
            print(line)
            ret+=line
            line = csvfile.readline()
    if headers=="open":
        ret=ret.replace("km,", "")
        ret=ret.replace("km", "")
        ret=ret.replace("Close,", "")
        ret=ret.replace("Close", "")
    elif headers=="close":
        ret=ret.replace("km,", "")
        ret=ret.replace("km", "")
        ret=ret.replace("Open,", '')
        ret=ret.replace("Open", '')
    return ret

@app.route("/")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('home.html')

class brevet(Resource):
    def __init__(self, **kwargs):
        self.csv=kwargs['csv']
        self.x = flask.request.args.get("top", type=int)

    def get(self):
        output=[]
        if self.x==None:
            _items = db.brev.find()
        else:
            _items = db.brev.find().limit(self.x)
        items = [item for item in _items]
        for item in items:
            item.pop('_id')
            output.append(item)
        if self.csv:
            return  output_csv(items, 200, "brevet")
        ret=json.dumps(str(output))
        ##flask.render_template('index.html', response=ret)
        return ret


class listOpenOnly(Resource):
    def __init__(self, **kwargs):
        self.csv=kwargs['csv']
        self.x=flask.request.args.get("top", type=int)

    def get(self):
        output=[]
        if self.x == None:
            _items = db.brev.find()
        else:
            _items = db.brev.find().limit(self.x)
        items = [item for item in _items]
        for item in items:
            item.pop('_id')
            output.append(item["Open"])
        if self.csv:
            return output_csv(items, 200, "open")
        ret = json.dumps(str(output))
        return ret



class listCloseOnly(Resource):
    def __init__(self, **kwargs):
        self.csv=kwargs['csv']
        self.x=flask.request.args.get("top", type=int)

    def get(self):
        output=[]
        if self.x == None:
            _items = db.brev.find()
        else:
            _items = _items = db.brev.find().limit(self.x)
        items = [item for item in _items]
        for item in items:
            item.pop('_id')
            output.append(item['Close'])
        if self.csv:
            output= output_csv(items, 200, "close")
        ret = json.dumps(str(output))
        return ret

# Create routes
# Another way, without decorators


api.add_resource(brevet, "/listAll/",  endpoint='listAll', resource_class_kwargs={'csv': False})
api.add_resource(brevet, "/listAll/json/",  endpoint='listAll-json',resource_class_kwargs={'csv': False} )
api.add_resource(listOpenOnly, '/listOpenOnly/', endpoint="listOpenOnly", resource_class_kwargs={'csv': False})
api.add_resource(listOpenOnly,"/listOpenOnly/json/", endpoint="listOpenOnly-json", resource_class_kwargs={'csv': False})
api.add_resource(listCloseOnly, '/listCloseOnly/', endpoint= "listCloseOnly", resource_class_kwargs={'csv': False})
api.add_resource(listCloseOnly,"/listCloseOnly/json/", endpoint='ListCloseOnly-json', resource_class_kwargs={'csv': False})
api.add_resource(brevet, "/listAll/csv/", resource_class_kwargs={'csv': True})
api.add_resource(listOpenOnly, "/listOpenOnly/csv/", resource_class_kwargs={'csv': True})
api.add_resource(listCloseOnly, "/listCloseOnly/csv/",resource_class_kwargs={'csv': True})
api.add_resource(brevet, "/listAll/?top={x}",  endpoint='listall-count', resource_class_kwargs={'csv': False})
api.add_resource(brevet, "/listAll/json/?top={x}",  endpoint='listAll-json-count',resource_class_kwargs={'csv': False} )
api.add_resource(listOpenOnly, '/listOpenOnly/?top={x}', endpoint="listOpenOnly-count", resource_class_kwargs={'csv': False})
api.add_resource(listOpenOnly,"/listOpenOnly/json/?top={x}", endpoint="listOpenOnly-json-count", resource_class_kwargs={'csv': False})
api.add_resource(listCloseOnly, '/listCloseOnly/?top={x}', endpoint= "listCloseOnly-count", resource_class_kwargs={'csv': False})
api.add_resource(listCloseOnly,"/listCloseOnly/json/?top={x}", endpoint='ListCloseOnly-json-count', resource_class_kwargs={'csv': False})
api.add_resource(brevet, "/listAll/csv/?top={x}", endpoint="listall-csv-count", resource_class_kwargs={'csv': True})
api.add_resource(listOpenOnly, "/listOpenOnly/csv/?top={x}",endpoint="listopenonly-csv-count", resource_class_kwargs={'csv': True})
api.add_resource(listCloseOnly, "/listCloseOnly/csv/?top={x}",endpoint="liscloseone-csv-count",resource_class_kwargs={'csv': True})


# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
